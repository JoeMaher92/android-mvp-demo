package kwickie.com.kwickie_android_mvp.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by joemaher on 30/04/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PreProfilePicture {

    @JsonProperty("url")
    private String uploadUrl;

    public PreProfilePicture() {
    }

    public PreProfilePicture(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }
}
