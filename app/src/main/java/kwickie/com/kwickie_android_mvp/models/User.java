package kwickie.com.kwickie_android_mvp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by joemaher on 30/04/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("profilePicturePath")
    private String profilePicture;
    @JsonProperty("coverImage")
    private String coverImage;
    @JsonProperty("celebVerified")
    private Boolean celebVerified;
    @JsonProperty("directKwickieBlocked")
    private Boolean directKwickieBlocked;
    @JsonProperty("email")
    private String email;
    @JsonProperty("featured")
    private Integer featured;
    @JsonProperty("id")
    private Long userId;
    @JsonProperty("bioText")
    private String bioText;
    @JsonProperty("mixpanelId")
    private String mixpanelId;
    @JsonProperty("followeesCount")
    private Integer followeesCount;
    @JsonProperty("followersCount")
    private Integer followersCount;
    @JsonIgnore
    private String password;
    @JsonProperty("following")
    private Boolean following;

    public User() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public Boolean getCelebVerified() {
        return celebVerified;
    }

    public void setCelebVerified(Boolean celebVerified) {
        this.celebVerified = celebVerified;
    }

    public Boolean getDirectKwickieBlocked() {
        return directKwickieBlocked;
    }

    public void setDirectKwickieBlocked(Boolean directKwickieBlocked) {
        this.directKwickieBlocked = directKwickieBlocked;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getBioText() {
        return bioText;
    }

    public void setBioText(String bioText) {
        this.bioText = bioText;
    }

    public String getMixpanelId() {
        return mixpanelId;
    }

    public void setMixpanelId(String mixpanelId) {
        this.mixpanelId = mixpanelId;
    }

    public Integer getFolloweesCount() {
        return followeesCount;
    }

    public void setFolloweesCount(Integer followeesCount) {
        this.followeesCount = followeesCount;
    }

    public Integer getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(Integer followersCount) {
        this.followersCount = followersCount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getFollowing() {
        return following;
    }

    public void setFollowing(Boolean following) {
        this.following = following;
    }
}
