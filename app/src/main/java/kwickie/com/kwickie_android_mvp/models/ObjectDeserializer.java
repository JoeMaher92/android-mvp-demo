package kwickie.com.kwickie_android_mvp.models;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Generic object deserializer used for volley requests
 */
public class ObjectDeserializer {

    public interface ObjectDeserializerListener<Type>{
        void onDeserializeObject(@Nullable Type object);
    }

    public interface ListDeserializerListener<Type>{
        void onDeserializeList(ArrayList<Type> list);
    }

    public static <Type> Type deserializeObjectSynchronous(@NonNull final String json, @NonNull final Class<Type> typeClass){
        ObjectMapper objectMapper = new ObjectMapper();
        Type object = null;
        try {
            object = objectMapper.readValue(json, typeClass);
        } catch (IOException e) {
        }

        return object;
    }

    public static <Type> ArrayList<Type> deserializeListSynchronous(@NonNull final String json, @NonNull final Class<Type> typeClass){
        ObjectMapper objectMapper = new ObjectMapper();
        JavaType type = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, typeClass);

        ArrayList<Type> list = null;
        try {
            list = objectMapper.readValue(json, type);
        } catch (IOException e) {
        }

        return list == null ? new ArrayList<Type>() : list;
    }

    public static <Type> void deserializeObject(@NonNull final String json, @NonNull final Class<Type> typeClass, @NonNull final ObjectDeserializerListener<Type> listener){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ObjectMapper objectMapper = new ObjectMapper();
                Type object = null;
                try {
                    object = objectMapper.readValue(json, typeClass);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    final Type finalObject = object;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onDeserializeObject(finalObject);
                        }
                    });
                }
            }
        }).start();
    }

    public static <Type> void deserializeList(@NonNull final String json, @NonNull final Class<Type> typeClass, @NonNull final ListDeserializerListener<Type> listener){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ObjectMapper objectMapper = new ObjectMapper();
                JavaType type = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, typeClass);

                ArrayList<Type> list = null;
                try {
                    list = objectMapper.readValue(json, type);
                } catch (IOException e) {
                } finally {
                    final ArrayList<Type> finalList = list == null ? new ArrayList<Type>() : list;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onDeserializeList(finalList);
                        }
                    });
                }
            }
        }).start();
    }
}
