package kwickie.com.kwickie_android_mvp.Helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

/**
 * Created by joemaher on 16/04/16.
 */
public class GalleryPermissionHelper extends PermissionsHelper {

    public static boolean hasPermission(Context context){
        return getPermissionState(context) == PermissionState.HAS_ALL;
    }

    public static void requestPermission(final Activity activity){

        PermissionState state = getPermissionState(activity);

        if (shouldShowRationale(activity)){
            createAndShowRationale(activity, state);
        } else {
            delegatePermissionRequest(activity, state);
        }
    }

    public static void delegatePermissionRequest(Activity activity, PermissionState currentState){

        switch (currentState) {

            case HAS_NONE:

                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_GALLERY);
                break;

            case HAS_ALL:
                break;
        }
    }

    private static boolean shouldShowRationale(Activity activity){
        return shouldShowRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private static void createAndShowRationale(final Activity activity, final PermissionState state){
        new AlertDialog.Builder(activity)
                .setTitle("Permission")
                .setMessage("I just need it")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delegatePermissionRequest(activity, state);
                    }
                })
                .create()
                .show();
    }

    private static PermissionState getPermissionState(Context context){

        if (hasReadExternalStoragePermission(context)){
            return PermissionState.HAS_ALL;
        }

        return PermissionState.HAS_NONE;
    }

    private enum PermissionState {
        HAS_NONE, HAS_ALL
    }
}
