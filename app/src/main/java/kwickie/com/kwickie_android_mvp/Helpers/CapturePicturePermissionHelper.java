package kwickie.com.kwickie_android_mvp.Helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

/**
 * Created by joemaher on 16/04/16.
 */
public class CapturePicturePermissionHelper extends PermissionsHelper {

    public static boolean hasPermission(Context context){
        return getPermissionState(context) == PermissionState.HAS_ALL;
    }

    public static void requestPermission(Activity activity){

        PermissionState state = getPermissionState(activity);

        if (shouldShowRationale(activity)){
            createAndShowRationale(activity, state);
        } else {
            delegatePermissionRequest(activity, state);
        }
    }

    public static void delegatePermissionRequest(Activity activity, PermissionState currentState){

        switch (currentState) {

            case HAS_NONE:

                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_CAPTURE_PICTURE);
                break;

            case HAS_CAMERA:

                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_CAPTURE_PICTURE);
                break;

            case HAS_WRITE_EXTERNAL_STORAGE:

                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_PERMISSION_CAPTURE_PICTURE);
                break;

            case HAS_ALL:
                break;
        }
    }

    private static boolean shouldShowRationale(Activity activity){

        boolean showCameraRationale = shouldShowRationale(activity, Manifest.permission.CAMERA);
        boolean showWriteExternalStorageRationale = shouldShowRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return showCameraRationale || showWriteExternalStorageRationale;
    }

    private static void createAndShowRationale(final Activity activity, final PermissionState state){
        new AlertDialog.Builder(activity)
                .setTitle("Permission")
                .setMessage("I just need it")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delegatePermissionRequest(activity, state);
                    }
                })
                .create()
                .show();
    }

    private static PermissionState getPermissionState(Context context){

        boolean hasCameraPermission = hasCameraPermission(context);
        boolean hasWriteExternalStoragePermission = hasWriteExternalStoragePermission(context);

        if (hasCameraPermission && hasWriteExternalStoragePermission){
            return PermissionState.HAS_ALL;
        } else if (hasCameraPermission){
            return PermissionState.HAS_CAMERA;
        } else if (hasWriteExternalStoragePermission){
            return PermissionState.HAS_WRITE_EXTERNAL_STORAGE;
        }

        return PermissionState.HAS_NONE;
    }

    private enum PermissionState {
        HAS_NONE, HAS_CAMERA, HAS_WRITE_EXTERNAL_STORAGE, HAS_ALL
    }
}
