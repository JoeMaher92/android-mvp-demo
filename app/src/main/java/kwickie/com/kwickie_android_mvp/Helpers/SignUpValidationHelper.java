package kwickie.com.kwickie_android_mvp.Helpers;

import android.util.Patterns;

/**
 * Created by joe on 25/01/2016.
 */
public class SignUpValidationHelper {

    public static final int PASSWORD_MIN_LENGTH = 8;

    public static boolean isValidName(String name){
        return !name.isEmpty() && !name.replace(" ", "").isEmpty();
    }

    public static boolean isValidEmail(String email){
        return !email.isEmpty() && !email.replace(" ", "").isEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(String password){
        return password.length() >= PASSWORD_MIN_LENGTH;
    }
}
