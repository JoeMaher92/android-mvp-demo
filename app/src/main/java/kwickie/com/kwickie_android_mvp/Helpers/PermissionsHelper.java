package kwickie.com.kwickie_android_mvp.Helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by joemaher on 22/02/16.
 */
public class PermissionsHelper {

    public final static int REQUEST_CODE_PERMISSION_CAPTURE_PICTURE = 0;
    public final static int REQUEST_CODE_PERMISSION_GALLERY = 1;
    public final static int REQUEST_CODE_PERMISSION_RECORDING = 2;

    protected static boolean hasPermission(Context context, String permission){
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    protected static boolean hasCameraPermission(Context context){
        return PermissionsHelper.hasPermission(context, Manifest.permission.CAMERA);
    }

    protected static boolean hasRecordAudioPermission(Context context){
        return PermissionsHelper.hasPermission(context, Manifest.permission.RECORD_AUDIO);
    }

    protected static boolean hasWriteExternalStoragePermission(Context context){
        return PermissionsHelper.hasPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    protected static boolean hasReadExternalStoragePermission(Context context){
        return PermissionsHelper.hasPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    protected static boolean shouldShowRationale(Activity activity, String permission){
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
    }

    public static boolean isPermissionGranted(int[] grantResults){
        for (int permissionStatus : grantResults){
            if (permissionStatus != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }

        return true;
    }
}
