package kwickie.com.kwickie_android_mvp.activites;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import kwickie.com.kwickie_android_mvp.Helpers.BitmapHelper;
import kwickie.com.kwickie_android_mvp.Helpers.CapturePicturePermissionHelper;
import kwickie.com.kwickie_android_mvp.Helpers.GalleryPermissionHelper;
import kwickie.com.kwickie_android_mvp.R;
import kwickie.com.kwickie_android_mvp.fragments.SignUpCredentialsFragment;
import kwickie.com.kwickie_android_mvp.fragments.SignUpNameFragment;
import kwickie.com.kwickie_android_mvp.interactors.MemberInteractorImpl;
import kwickie.com.kwickie_android_mvp.interactors.ServiceInteractorImpl;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.SignUpPresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpChildView;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpView;
import kwickie.com.kwickie_android_mvp.presenters.SignUpPresenterImpl;

/**
 * Created by joemaher on 30/04/16.
 */
public class SignUpActivity extends AppCompatActivity implements SignUpView {

    private static final String NAME_FRAGMENT_TAG = "SignUpNameFragment";
    private static final String CREDENTIALS_FRAGMENT_TAG = "SignUpCredentialsFragment";
    public static final String FRAGMENT_ARG_EMAIL = "SignUpEmailArg";
    public static final String FRAGMENT_ARG_PASSWORD = "SignUpPasswordArg";

    private static final int RETURNING_TO_BASE = 1;
    private static final int RETURNING_TO_POPULATED_BACK_STACK = 2;
    private static final int ACTIVITY_REQUEST_CAMERA = 6;
    private static final int ACTIVITY_REQUEST_GALLERY = 7;

    private SignUpPresenter mPresenter;
    private String mFirstName;
    private String mLastName;
    private File mProfileImageFile;
    private String mEmail;
    private String mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mPresenter = new SignUpPresenterImpl(this, new MemberInteractorImpl(getApplicationContext()), new ServiceInteractorImpl(getApplicationContext()));
        mPresenter.create();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mPresenter.premissionsRequestResult(requestCode, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ACTIVITY_REQUEST_CAMERA) {

            if (resultCode == RESULT_OK) {
                mPresenter.imageCaptured(null);
            }

        } else if (requestCode == ACTIVITY_REQUEST_GALLERY) {

            if (resultCode == RESULT_OK) {

                Bitmap bitmap = BitmapHelper.loadBitmapFromGallery(this, data);

                if (bitmap != null){

                    try {

                        File imageFile = BitmapHelper.createFileFromBitmap(this, bitmap);
                        mPresenter.galleryImageSelected(imageFile);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_profile_image, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_camera:
                mPresenter.cameraRequested(CapturePicturePermissionHelper.hasPermission(this));
                return true;

            case R.id.action_gallery:
                mPresenter.galleryRequested(GalleryPermissionHelper.hasPermission(this));
                return true;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        FragmentManager manager = getSupportFragmentManager();
        int stackCount = manager.getBackStackEntryCount();

        if (stackCount == RETURNING_TO_BASE){

            Fragment previousFragment = manager.findFragmentByTag(NAME_FRAGMENT_TAG);
            mPresenter.setChildView((SignUpChildView) previousFragment);

        } else if (stackCount >= RETURNING_TO_POPULATED_BACK_STACK) { //never used in this activity, just demonstrating how we would do its

            String previousFragmentTag = manager.getBackStackEntryAt(stackCount - 1).getName();
            Fragment previousFragment = manager.findFragmentByTag(previousFragmentTag);

            mPresenter.setChildView((SignUpChildView) previousFragment);
        }

        super.onBackPressed();
    }

    public void onProfilePicturePressed(View v){
        registerForContextMenu(v);
        openContextMenu(v);
    }

    public void onTermsPressed(View v){
        mPresenter.termsClicked();
    }

    public void onPrivacyPressed(View v){
        mPresenter.privacyClicked();
    }

    @Override
    public void requestCapturePicturePermission() {
        CapturePicturePermissionHelper.requestPermission(this);
    }

    @Override
    public void requestGalleryPermission() {
        GalleryPermissionHelper.requestPermission(this);
    }

    @Override
    public void showCamera() {
        Toast.makeText(this, "Removed for Simplicity", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");

        startActivityForResult(photoPickerIntent, ACTIVITY_REQUEST_GALLERY);
    }

    @Override
    public void showTerms() {
        Toast.makeText(this, "Terms Pressed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPrivacy() {
        Toast.makeText(this, "Privacy Pressed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmailInUse() {
        Toast.makeText(this, "Email is already in use", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProfilePictureRequired() {
        Toast.makeText(this, "Profile picture is required", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showCredentialsSection() {

        final SignUpCredentialsFragment fragment = new SignUpCredentialsFragment();

        if(mEmail != null || mPassword != null) {

            final Bundle bundle = new Bundle();

            if (mEmail != null){
                bundle.putString(FRAGMENT_ARG_EMAIL, mEmail);
            }

            if (mPassword != null){
                bundle.putString(FRAGMENT_ARG_PASSWORD, mPassword);
            }

            fragment.setArguments(bundle);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, CREDENTIALS_FRAGMENT_TAG)
                .addToBackStack(CREDENTIALS_FRAGMENT_TAG)
                .commit();

        mPresenter.setChildView(fragment);
    }

    @Override
    public void showNameSection() {

        final SignUpNameFragment fragment = new SignUpNameFragment();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, fragment, NAME_FRAGMENT_TAG)
                .disallowAddToBackStack()
                .commit();

        mPresenter.setChildView(fragment);
    }

    @Override
    public void finaliseSignUp() {
        mPresenter.registerUser(mFirstName, mLastName, mEmail, mPassword, mProfileImageFile);
    }

    @Override
    public void saveNames(String firstName, String lastName) {
        mFirstName = firstName;
        mLastName = lastName;
    }

    @Override
    public void saveCredentials(String email, String password) {
        mEmail = email;
        mPassword = password;
    }

    @Override
    public void saveProfileImageFile(File profileImageFile) {
        mProfileImageFile = profileImageFile;
    }

    @Override
    public void displayProfileImageInChild() {
        mPresenter.displayProfileImage(mProfileImageFile);
    }

    @Override
    public void showMainActivity() {
        Toast.makeText(this, "Registration Successful", Toast.LENGTH_LONG).show();
    }

    @Override
    public void cameraPermissionDenied() {
        Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_LONG).show();
    }

    @Override
    public void galleryPermissionDenied() {
        Toast.makeText(this, "Gallery Permission Denied", Toast.LENGTH_LONG).show();
    }
}
