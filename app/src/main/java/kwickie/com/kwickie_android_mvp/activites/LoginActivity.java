package kwickie.com.kwickie_android_mvp.activites;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import kwickie.com.kwickie_android_mvp.R;
import kwickie.com.kwickie_android_mvp.interactors.MemberInteractorImpl;
import kwickie.com.kwickie_android_mvp.interactors.SharedPreferencesInteractorImpl;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.LoginPresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.LoginView;
import kwickie.com.kwickie_android_mvp.presenters.LoginPresenterImpl;

/**
 * Created by joemaher on 30/04/16.
 */
public class LoginActivity extends AppCompatActivity implements LoginView {

    private EditText mEmailText;
    private EditText mPasswordText;
    private ImageView mReveal;
    private LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPresenter = new LoginPresenterImpl(this, new MemberInteractorImpl(getApplicationContext()), new SharedPreferencesInteractorImpl(getApplicationContext()));

        mEmailText = (EditText) findViewById(R.id.email);
        mPasswordText = (EditText) findViewById(R.id.password);
        mReveal = (ImageView) findViewById(R.id.reveal);
        mPasswordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE ||
                        actionId == EditorInfo.IME_ACTION_GO) {
                    onLoginPressed(null);
                    return true;
                }
                return false;
            }
        });


    }

    @Override
    protected void onDestroy() {
        mPresenter.destory();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_login:
                onLoginPressed(null);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void revealPasswordPressed(View v) {

        int startPos = mPasswordText.getSelectionStart();
        int endPos = mPasswordText.getSelectionEnd();
        boolean transformed = mPasswordText.getTransformationMethod() instanceof PasswordTransformationMethod;

        mPresenter.revealPasswordClicked(startPos, endPos, transformed);
    }

    public void forgotPasswordPressed(View v) {
        mPresenter.forgotPasswordClicked();
    }

    public void onLoginPressed(View v) {

        final String email = mEmailText.getText().toString();
        final String password = mPasswordText.getText().toString();

        mPresenter.validateCredentials(email, password);
    }

    @Override
    public void revealPassword(int startPos, int endPos) {

        mPasswordText.setTransformationMethod(null);
        mPasswordText.setSelection(startPos, endPos);
        mReveal.setImageResource(R.drawable.ic_visibility_off);
    }

    @Override
    public void hidePassword(int startPos, int endPos) {

        mPasswordText.setTransformationMethod(new PasswordTransformationMethod());
        mPasswordText.setSelection(startPos, endPos);
        mReveal.setImageResource(R.drawable.ic_visibility);
    }

    @Override
    public void showForgotPassword() {
        Toast.makeText(this, "Forgot password clicked", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMainActivity() {
        Toast.makeText(this, "Successfully logged in", Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayLoginError(String message) {
        Toast.makeText(this, "Invalid Login", Toast.LENGTH_LONG).show();
    }

    @Override
    public void loginRequestFinished() {

    }

    @Override
    public void malformedCredentials() {
        Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_LONG).show();
    }
}
