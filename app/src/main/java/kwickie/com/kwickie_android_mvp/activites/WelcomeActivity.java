package kwickie.com.kwickie_android_mvp.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kwickie.com.kwickie_android_mvp.R;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.WelcomePresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.WelcomeView;
import kwickie.com.kwickie_android_mvp.presenters.WelcomePresenterImpl;
import kwickie.com.kwickie_android_mvp.requests.VolleySingleton;

/**
 * Created by joemaher on 30/04/16.
 */
public class WelcomeActivity extends Activity implements WelcomeView {

    private WelcomePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        VolleySingleton.init(this);

        mPresenter = new WelcomePresenterImpl(this);
        mPresenter.create();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onSignUpPressed(View v){
        mPresenter.signUpClicked();
    }

    public void onLoginPressed(View v){
        mPresenter.loginClicked();
    }

    @Override
    public void showSignUp() {
        Intent i = new Intent(this, SignUpActivity.class);
        startActivity(i);
    }

    @Override
    public void showLogin() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }
}
