package kwickie.com.kwickie_android_mvp.interfaces.views;

/**
 * Created by joemaher on 30/04/16.
 */
public interface LoginView {

    void revealPassword(int startPos, int endPos);

    void hidePassword(int startPos, int endPos);

    void showForgotPassword();

    void showMainActivity();

    void displayLoginError(String message);

    void loginRequestFinished();

    void malformedCredentials();
}
