package kwickie.com.kwickie_android_mvp.interfaces.views;

/**
 * Created by joemaher on 30/04/16.
 */
public interface WelcomeView {

    void showLogin();

    void showSignUp();
}
