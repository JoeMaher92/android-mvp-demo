package kwickie.com.kwickie_android_mvp.interfaces.presenters;

/**
 * Created by joemaher on 2/05/16.
 */
public interface SignUpCredentialsPresenter {

    void createView(String email, String password);

    void revealPasswordClicked(int startPos, int endPos, boolean hidden);

    void validateCredentials(String email, String password);

    void destroyView(String email, String password);
}
