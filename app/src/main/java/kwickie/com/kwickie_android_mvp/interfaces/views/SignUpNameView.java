package kwickie.com.kwickie_android_mvp.interfaces.views;

/**
 * Created by joemaher on 1/05/16.
 */
public interface SignUpNameView extends SignUpChildView {

    void onNextPressed();

    void showFirstNameInvalid();

    void showLastNameInvalid();
}
