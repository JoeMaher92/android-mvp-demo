package kwickie.com.kwickie_android_mvp.interfaces.presenters;

/**
 * Created by joemaher on 1/05/16.
 */
public interface SignUpNamePresenter {

    void createView();

    void validateNames(String firstName, String lastName);

    void destroyView();
}
