package kwickie.com.kwickie_android_mvp.interfaces.views;

/**
 * Created by joemaher on 2/05/16.
 */
public interface SignUpCredentialsView extends SignUpChildView {

    void onContinuePressed();

    void revealPassword(int startPos, int endPos);

    void hidePassword(int startPos, int endPos);

    void showEmailInvalid();

    void showPasswordInvalid();

    void fillEmail(String email);

    void fillPassword(String password);
}

