package kwickie.com.kwickie_android_mvp.interfaces.views;

import java.io.File;

/**
 * Created by joemaher on 30/04/16.
 */
public interface SignUpView {

    void requestCapturePicturePermission();

    void requestGalleryPermission();

    void showCamera();

    void showGallery();

    void showTerms();

    void showPrivacy();

    void showEmailInUse();

    void showProfilePictureRequired();

    void showCredentialsSection();

    void showNameSection();

    void finaliseSignUp();

    void saveNames(String firstName, String lastName);

    void saveCredentials(String email, String password);

    void saveProfileImageFile(File profileImageFile);

    void displayProfileImageInChild();

    void showMainActivity();

    void cameraPermissionDenied();

    void galleryPermissionDenied();
}
