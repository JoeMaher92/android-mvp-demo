package kwickie.com.kwickie_android_mvp.interfaces.interactors;

import java.io.File;

import kwickie.com.kwickie_android_mvp.models.PreProfilePicture;
import kwickie.com.kwickie_android_mvp.models.TokenResponse;
import kwickie.com.kwickie_android_mvp.models.User;

/**
 * Created by joemaher on 30/04/16.
 */
public interface MemberInteractor {

    void login(LoginUserListener loginUserListener, String email, String password);

    void register(RegisterUserListener registerUserListener, User user, File profilePictureFile);

    interface LoginUserListener {
        void onSuccess(TokenResponse tokenResponse);
        void onError(String error);
        void onFinished();
    }

    interface RegisterUserListener {
        void onSuccess(PreProfilePicture preProfilePicture, File profilePictureFile);
        void onEmailInUse();
        void onError(String error);
        void onFinished();
    }
}
