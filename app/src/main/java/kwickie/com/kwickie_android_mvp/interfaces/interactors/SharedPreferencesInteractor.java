package kwickie.com.kwickie_android_mvp.interfaces.interactors;

import kwickie.com.kwickie_android_mvp.models.TokenResponse;

/**
 * Created by joemaher on 30/04/16.
 */
public interface SharedPreferencesInteractor {

    void storeToken(TokenResponse tokenResponse);
}
