package kwickie.com.kwickie_android_mvp.interfaces.presenters;

import java.io.File;

import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpChildView;

/**
 * Created by joemaher on 30/04/16.
 */
public interface SignUpPresenter {

    void create();

    void registerUser(String firstName, String lastName, String email, String password, File profilePictureFile);

    void privacyClicked();

    void termsClicked();

    void premissionsRequestResult(int requestCode, int[] grantResults);

    void imageCaptured(File file);

    void galleryImageSelected(File file);

    void displayProfileImage(File profileImageFile);

    void cameraRequested(boolean hasPermission);

    void galleryRequested(boolean hasPermission);

    void setChildView(SignUpChildView signUpChildView);

    void destroy();
}
