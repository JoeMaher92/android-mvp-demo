package kwickie.com.kwickie_android_mvp.interfaces.interactors;

import java.io.File;

import kwickie.com.kwickie_android_mvp.models.PreProfilePicture;

/**
 * Created by joemaher on 30/04/16.
 */
public interface ServiceInteractor {

    void uploadProfileImage(PreProfilePicture preProfilePicture, File imageFile);
}
