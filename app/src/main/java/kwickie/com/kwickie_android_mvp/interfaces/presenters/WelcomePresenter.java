package kwickie.com.kwickie_android_mvp.interfaces.presenters;

/**
 * Created by joemaher on 30/04/16.
 */
public interface WelcomePresenter {

    void create();

    void signUpClicked();

    void loginClicked();

    void destroy();
}
