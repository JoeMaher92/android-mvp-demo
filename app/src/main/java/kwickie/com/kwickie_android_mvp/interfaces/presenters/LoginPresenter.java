package kwickie.com.kwickie_android_mvp.interfaces.presenters;

/**
 * Created by joemaher on 30/04/16.
 */
public interface LoginPresenter {

    void create();

    void revealPasswordClicked(int startPos, int endPos, boolean hidden);

    void forgotPasswordClicked();

    void validateCredentials(String email, String password);

    void destory();
}
