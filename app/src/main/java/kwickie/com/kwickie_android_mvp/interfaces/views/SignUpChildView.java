package kwickie.com.kwickie_android_mvp.interfaces.views;

/**
 * Created by joemaher on 1/05/16.
 */
public interface SignUpChildView {

    void displayProfileImage(String profileImageFilePath);

    void resetProfileImage();

    SignUpView getParentView();
}
