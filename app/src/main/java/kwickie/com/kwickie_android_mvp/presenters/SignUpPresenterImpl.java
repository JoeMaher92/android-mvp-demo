package kwickie.com.kwickie_android_mvp.presenters;

import java.io.File;

import kwickie.com.kwickie_android_mvp.Helpers.PermissionsHelper;
import kwickie.com.kwickie_android_mvp.interfaces.interactors.MemberInteractor;
import kwickie.com.kwickie_android_mvp.interfaces.interactors.ServiceInteractor;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.SignUpPresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpChildView;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpView;
import kwickie.com.kwickie_android_mvp.models.PreProfilePicture;
import kwickie.com.kwickie_android_mvp.models.User;

/**
 * Created by joemaher on 30/04/16.
 */
public class SignUpPresenterImpl implements SignUpPresenter, MemberInteractor.RegisterUserListener {

    private SignUpView mView;
    private SignUpChildView mChildView;
    private MemberInteractor mMemberInteractor;
    private ServiceInteractor mServiceInteractor;

    public SignUpPresenterImpl(SignUpView signUpView, MemberInteractor memberInteractor, ServiceInteractor serviceInteractor) {
        mView = signUpView;
        mMemberInteractor = memberInteractor;
        mServiceInteractor = serviceInteractor;
    }

    @Override
    public void create() {
        mView.showNameSection();
    }

    @Override
    public void registerUser(String firstName, String lastName, String email, String password, File profilePictureFile) {

        if (profilePictureFile == null){
            mView.showProfilePictureRequired();
        } else {

            User user = new User();

            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setPassword(password);

            mMemberInteractor.register(this, user, profilePictureFile);
        }
    }

    @Override
    public void privacyClicked() {
        mView.showPrivacy();
    }

    @Override
    public void termsClicked() {
        mView.showTerms();
    }

    @Override
    public void premissionsRequestResult(int requestCode, int[] grantResults) {
        if (requestCode == PermissionsHelper.REQUEST_CODE_PERMISSION_CAPTURE_PICTURE) {
            if (PermissionsHelper.isPermissionGranted(grantResults)) {
                mView.showCamera();
            } else {
                mView.cameraPermissionDenied();
            }
        } else if (requestCode == PermissionsHelper.REQUEST_CODE_PERMISSION_GALLERY) {
            if (PermissionsHelper.isPermissionGranted(grantResults)) {
                mView.showGallery();
            } else {
                mView.galleryPermissionDenied();
            }
        }
    }

    @Override
    public void imageCaptured(File file) {

    }

    @Override
    public void galleryImageSelected(File file) {

        mView.saveProfileImageFile(file);
        displayProfileImage(file);
    }

    @Override
    public void displayProfileImage(File profileImageFile) {

        if (profileImageFile != null){
            mChildView.displayProfileImage(profileImageFile.getAbsolutePath());
        }
    }

    @Override
    public void cameraRequested(boolean hasPermission) {
        if (hasPermission){
            mView.showCamera();
        } else {
            mView.requestCapturePicturePermission();
        }
    }

    @Override
    public void galleryRequested(boolean hasPermission) {

        if (hasPermission){
            mView.showGallery();
        } else {
            mView.requestGalleryPermission();
        }
    }

    @Override
    public void setChildView(SignUpChildView signUpChildView) {
        mChildView = signUpChildView;
    }

    @Override
    public void destroy() {
        mMemberInteractor = null;
        mServiceInteractor = null;
        mView = null;
    }

    @Override
    public void onSuccess(PreProfilePicture preProfilePicture, File profilePictureFile) {
        mServiceInteractor.uploadProfileImage(preProfilePicture, profilePictureFile);
        mView.showMainActivity();
    }

    @Override
    public void onEmailInUse() {
        mView.showEmailInUse();
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onFinished() {
        mView.showMainActivity();
    }
}
