package kwickie.com.kwickie_android_mvp.presenters;

import kwickie.com.kwickie_android_mvp.interfaces.presenters.WelcomePresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.WelcomeView;

/**
 * Created by joemaher on 30/04/16.
 */
public class WelcomePresenterImpl implements WelcomePresenter {

    private WelcomeView mView;

    public WelcomePresenterImpl(WelcomeView welcomeView) {
        mView = welcomeView;
    }

    @Override
    public void create() {

    }

    @Override
    public void signUpClicked() {
        mView.showSignUp();
    }

    @Override
    public void loginClicked() {
        mView.showLogin();
    }

    @Override
    public void destroy() {
        mView = null;
    }
}
