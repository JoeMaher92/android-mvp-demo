package kwickie.com.kwickie_android_mvp.presenters;

import kwickie.com.kwickie_android_mvp.Helpers.SignUpValidationHelper;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.SignUpCredentialsPresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpCredentialsView;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpView;

/**
 * Created by joemaher on 2/05/16.
 */
public class SignUpCredentialsPresenterImpl implements SignUpCredentialsPresenter {

    private SignUpCredentialsView mView;
    private SignUpView mParentView;

    public SignUpCredentialsPresenterImpl(SignUpCredentialsView signUpCredentialsView) {
        mView = signUpCredentialsView;
        mParentView = signUpCredentialsView.getParentView();
    }

    @Override
    public void createView(String email, String password) {

        mParentView.displayProfileImageInChild();

        if (email != null){
            mView.fillEmail(email);
        }

        if (password != null){
            mView.fillPassword(password);
        }

    }

    @Override
    public void revealPasswordClicked(int startPos, int endPos, boolean hidden) {
        if (hidden){
            mView.revealPassword(startPos, endPos);
        } else {
            mView.hidePassword(startPos, endPos);
        }
    }

    @Override
    public void validateCredentials(String email, String password) {

        boolean validEmail = SignUpValidationHelper.isValidEmail(email);
        boolean validPassword = SignUpValidationHelper.isValidPassword(password);

        if (validEmail && validPassword){
            mParentView.saveCredentials(email, password);
            mParentView.finaliseSignUp();
        } else {

            if (!validEmail) {
                mView.showEmailInvalid();
            }

            if (!validPassword){
                mView.showPasswordInvalid();
            }
        }
    }

    @Override
    public void destroyView(String email, String password) {

        mParentView.saveCredentials(email, password);

        mView = null;
        mParentView = null;
    }
}
