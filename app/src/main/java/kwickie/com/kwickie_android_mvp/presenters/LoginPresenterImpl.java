package kwickie.com.kwickie_android_mvp.presenters;

import kwickie.com.kwickie_android_mvp.interfaces.interactors.MemberInteractor;
import kwickie.com.kwickie_android_mvp.interfaces.interactors.SharedPreferencesInteractor;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.LoginPresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.LoginView;
import kwickie.com.kwickie_android_mvp.models.TokenResponse;

/**
 * Created by joemaher on 30/04/16.
 */
public class LoginPresenterImpl implements LoginPresenter, MemberInteractor.LoginUserListener {

    private LoginView mView;
    private MemberInteractor mMemberInteractor;
    private SharedPreferencesInteractor mSharedPreferencesInteractor;

    public LoginPresenterImpl(LoginView loginView, MemberInteractor memberInteractor, SharedPreferencesInteractor sharedPreferencesInteractor) {
        mView = loginView;
        mMemberInteractor = memberInteractor;
        mSharedPreferencesInteractor = sharedPreferencesInteractor;
    }

    @Override
    public void create() {

    }

    @Override
    public void revealPasswordClicked(int startPos, int endPos, boolean hidden) {

        if (hidden){
            mView.revealPassword(startPos, endPos);
        } else {
            mView.hidePassword(startPos, endPos);
        }
    }

    @Override
    public void forgotPasswordClicked() {
        mView.showForgotPassword();
    }

    @Override
    public void validateCredentials(String email, String password) {

        if (email.isEmpty() || password.isEmpty()) {
            mView.malformedCredentials();
            return;
        }

        mMemberInteractor.login(this, email, password);
    }

    @Override
    public void destory() {
        mView = null;
        mMemberInteractor = null;
    }

    @Override
    public void onSuccess(TokenResponse tokenResponse) {
        mSharedPreferencesInteractor.storeToken(tokenResponse);
        mView.showMainActivity();
    }

    @Override
    public void onError(String error) {
        mView.displayLoginError(error);
    }

    @Override
    public void onFinished() {
        mView.loginRequestFinished();
    }
}
