package kwickie.com.kwickie_android_mvp.presenters;

import kwickie.com.kwickie_android_mvp.Helpers.SignUpValidationHelper;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.SignUpNamePresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpNameView;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpView;

/**
 * Created by joemaher on 1/05/16.
 */
public class SignUpNamePresenterImpl implements SignUpNamePresenter {

    private SignUpNameView mView;
    private SignUpView mParentView;

    public SignUpNamePresenterImpl(SignUpNameView signUpNameView) {
        mParentView = signUpNameView.getParentView();
        mView = signUpNameView;
    }

    @Override
    public void createView() {
        mParentView.displayProfileImageInChild();
    }

    @Override
    public void validateNames(String firstName, String lastName) {

        boolean validFirstName = SignUpValidationHelper.isValidName(firstName);
        boolean validLastName = SignUpValidationHelper.isValidName(lastName);

        if (validFirstName && validLastName){
            mParentView.saveNames(firstName, lastName);
            mParentView.showCredentialsSection();
        } else {

            if (!validFirstName) {
                mView.showFirstNameInvalid();
            }

            if (!validLastName) {
                mView.showLastNameInvalid();
            }
        }
    }

    @Override
    public void destroyView() {
        mView = null;
        mParentView = null;
    }
}
