package kwickie.com.kwickie_android_mvp.interactors;

import android.content.Context;

import java.io.File;

import kwickie.com.kwickie_android_mvp.interfaces.interactors.MemberInteractor;
import kwickie.com.kwickie_android_mvp.models.User;
import kwickie.com.kwickie_android_mvp.requests.LoginUserRequest;
import kwickie.com.kwickie_android_mvp.requests.RegisterUserRequest;

/**
 * Created by joemaher on 30/04/16.
 */
public class MemberInteractorImpl implements MemberInteractor {

    private final Context mApplicationContext;

    public MemberInteractorImpl(Context applicationContext) {
        mApplicationContext = applicationContext;
    }

    @Override
    public void login(LoginUserListener loginUserListener, String email, String password) {
        new LoginUserRequest(loginUserListener, email, password).execute();
    }

    @Override
    public void register(RegisterUserListener registerUserListener, User user, File profilePictureFile) {
        new RegisterUserRequest(registerUserListener, user, profilePictureFile).execute();
    }
}
