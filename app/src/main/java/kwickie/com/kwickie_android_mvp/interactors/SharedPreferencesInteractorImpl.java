package kwickie.com.kwickie_android_mvp.interactors;

import android.content.Context;

import kwickie.com.kwickie_android_mvp.interfaces.interactors.SharedPreferencesInteractor;
import kwickie.com.kwickie_android_mvp.models.TokenResponse;

/**
 * Created by joemaher on 30/04/16.
 */
public class SharedPreferencesInteractorImpl implements SharedPreferencesInteractor {

    private final Context mApplicationContext;

    public SharedPreferencesInteractorImpl(Context applicationContext) {
        mApplicationContext = applicationContext;
    }

    @Override
    public void storeToken(TokenResponse tokenResponse) {
        //just an example
    }
}
