package kwickie.com.kwickie_android_mvp.interactors;

import android.content.Context;

import java.io.File;

import kwickie.com.kwickie_android_mvp.interfaces.interactors.ServiceInteractor;
import kwickie.com.kwickie_android_mvp.models.PreProfilePicture;

/**
 * Created by joemaher on 1/05/16.
 */
public class ServiceInteractorImpl implements ServiceInteractor {

    private final Context mApplicationContext;

    public ServiceInteractorImpl(Context applicationContext) {
        mApplicationContext = applicationContext;
    }

    @Override
    public void uploadProfileImage(PreProfilePicture preProfilePicture, File imageFile) {

    }
}
