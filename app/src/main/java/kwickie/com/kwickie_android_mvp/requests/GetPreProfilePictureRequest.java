package kwickie.com.kwickie_android_mvp.requests;

import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import kwickie.com.kwickie_android_mvp.models.ObjectDeserializer;
import kwickie.com.kwickie_android_mvp.models.PreProfilePicture;

/**
 * Created by joemaher on 16/02/16.
 */
public class GetPreProfilePictureRequest {

    public static String PRE_PROFILE_PICTURE_URL = "https://bigdev.kwickie.com/api/0/members/me/thumbnail";

    private AuthorisedStringRequest mRequest;
    private GetPreProfilePictureListener mListener;

    public interface GetPreProfilePictureListener {
        void onSuccess(PreProfilePicture preProfilePicture);
        void onError(String error);
        void onFinished();
    }

    public GetPreProfilePictureRequest(final GetPreProfilePictureListener listener){

        mListener = listener;
        mRequest = new AuthorisedStringRequest(Request.Method.POST, PRE_PROFILE_PICTURE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ObjectDeserializer.deserializeObject(response, PreProfilePicture.class, new ObjectDeserializer.ObjectDeserializerListener<PreProfilePicture>() {
                    @Override
                    public void onDeserializeObject(@Nullable PreProfilePicture object) {
                        mListener.onSuccess(object);
                        mListener.onFinished();
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                listener.onError(error.getMessage());
                listener.onFinished();
            }
        });
    }

    public void execute(){
        VolleySingleton.getInstance().getRequestQueue().add(mRequest);
    }

    public void cancel(){
        mRequest.cancel();
    }
}
