package kwickie.com.kwickie_android_mvp.requests;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

import kwickie.com.kwickie_android_mvp.interfaces.interactors.MemberInteractor;
import kwickie.com.kwickie_android_mvp.models.ObjectDeserializer;
import kwickie.com.kwickie_android_mvp.models.TokenResponse;

/**
 * Created by drewcunning on 16/02/16.
 */
public class LoginUserRequest {

    public static String LOGIN_URL = "https://bigdev.kwickie.com/api/0/members/login";

    private IdentifiedDeviceStringRequest mRequest;
    private MemberInteractor.LoginUserListener mListener;

    public LoginUserRequest(@NonNull MemberInteractor.LoginUserListener listener, final String emai, final String password){

        mListener = listener;
        mRequest = new IdentifiedDeviceStringRequest(Request.Method.POST, LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ObjectDeserializer.deserializeObject(response, TokenResponse.class, new ObjectDeserializer.ObjectDeserializerListener<TokenResponse>() {
                    @Override
                    public void onDeserializeObject(@Nullable TokenResponse object) {
                        mListener.onSuccess(object);
                        mListener.onFinished();
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mListener.onError(error.getMessage());
                mListener.onFinished();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("email", emai);
                map.put("password", password);

                return map;
            }
        };
    }

    public void execute(){
        VolleySingleton.getInstance().getRequestQueue().add(mRequest);
    }

    public void cancel(){
        mRequest.cancel();
    }
}
