package kwickie.com.kwickie_android_mvp.requests;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by joe on 12/02/2016.
 */
public class VolleySingleton {

    public static final String AUTHORISATION_HEADER = "Authorization";
    public static final String DEVICE_IDENTIFIER_HEADER = "X-Device-Identifier";

    public static final int BAD_REQUEST = 400;
    public static final int UNAUTHORISED = 401;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
    public static final int UNPROCESSABLE_ENTITY = 422;
    public static final int PROCESSED_NO_CONTENT = 204;
    public static final int INTERNAL_ERROR = 500;
    public static final int OK = 200;
    public static final int NO_INTERNET = -1;

    private RequestQueue requestQueue;
    private static VolleySingleton instance;
    private static String userToken;
    private static String deviceId;

    public static void init(Context applicationContext){
        instance = new VolleySingleton(applicationContext);
    }

    private VolleySingleton(Context context) {
        this.requestQueue = Volley.newRequestQueue(context);
    }

    public static VolleySingleton getInstance() {
        return instance;
    }

    public RequestQueue getRequestQueue(){
        return requestQueue;
    }

    public static String getUserToken() {
        return userToken;
    }

    public static void setUserToken(String userToken) {
        VolleySingleton.userToken = userToken;
    }

    public static String getDeviceId() {
        return deviceId;
    }

    public static void setDeviceId(String deviceId) {
        VolleySingleton.deviceId = deviceId;
    }
}


