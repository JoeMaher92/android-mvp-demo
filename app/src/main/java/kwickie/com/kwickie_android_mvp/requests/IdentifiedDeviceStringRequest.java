package kwickie.com.kwickie_android_mvp.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joemaher on 6/04/16.
 */
public class IdentifiedDeviceStringRequest extends StringRequest {

    public IdentifiedDeviceStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public IdentifiedDeviceStringRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        HashMap<String, String> headers = new HashMap<>();

        headers.put(VolleySingleton.DEVICE_IDENTIFIER_HEADER, VolleySingleton.getDeviceId());

        return headers;
    }
}
