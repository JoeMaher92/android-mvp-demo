package kwickie.com.kwickie_android_mvp.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joemaher on 9/03/16.
 */
public class AuthorisedStringRequest extends IdentifiedDeviceStringRequest {

    public AuthorisedStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);;
    }

    public AuthorisedStringRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        HashMap<String, String> headers = new HashMap<>(super.getHeaders());
        headers.put(VolleySingleton.AUTHORISATION_HEADER, VolleySingleton.getUserToken());
        return headers;
    }
}
