package kwickie.com.kwickie_android_mvp.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Custom image view for displaying user profile and brand images
 */
public class UserProfileImageView extends CircleImageView {

    public UserProfileImageView(Context context) {
        super(context);

        init(null);
    }

    public UserProfileImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public UserProfileImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    private void init(AttributeSet attributeSet){

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int measureWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        final int measureHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        final int finalSize;
        if(measureHeight > measureWidth){
            finalSize = measureWidth;
        } else{
            finalSize = measureHeight;
        }

        setMeasuredDimension(finalSize, finalSize);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
