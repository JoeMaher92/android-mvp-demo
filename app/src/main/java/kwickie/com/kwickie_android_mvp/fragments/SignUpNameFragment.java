package kwickie.com.kwickie_android_mvp.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import kwickie.com.kwickie_android_mvp.R;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.SignUpNamePresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpNameView;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpView;
import kwickie.com.kwickie_android_mvp.presenters.SignUpNamePresenterImpl;

/**
 * Created by joemaher on 30/04/16.
 */
public class SignUpNameFragment extends Fragment implements SignUpNameView {

    private SignUpNamePresenter mPresenter;
    private EditText mFirstNameText;
    private EditText mLastNameText;
    private ImageView mProfileImage;
    private ImageView mProfileImageEmpty;
    private ImageView mProfileImageEdit;

    public SignUpNameFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_sign_up_name, container, false);

        mPresenter = new SignUpNamePresenterImpl(this);

        mFirstNameText = (EditText) rootview.findViewById(R.id.first_name_text);
        mLastNameText = (EditText) rootview.findViewById(R.id.last_name_text);
        mProfileImage = (ImageView) rootview.findViewById(R.id.profile_image);
        mProfileImageEmpty = (ImageView) rootview.findViewById(R.id.profile_image_empty);
        mProfileImageEdit = (ImageView) rootview.findViewById(R.id.profile_image_edit);

        mLastNameText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_NEXT) {
                    onNextPressed();
                    return true;
                }

                return false;
            }
        });

        mPresenter.createView();

        return rootview;
    }

    @Override
    public void onDestroyView() {
        mPresenter.destroyView();
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sign_up_credentials, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_next:
                onNextPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNextPressed() {

        String firstName = mFirstNameText.getText().toString();
        String lastName = mLastNameText.getText().toString();

        mPresenter.validateNames(firstName, lastName);
    }

    @Override
    public void displayProfileImage(String profileImageFilePath) {

        Bitmap bitmap = BitmapFactory.decodeFile(profileImageFilePath);

        mProfileImage.setImageBitmap(bitmap);
        mProfileImageEmpty.setVisibility(View.INVISIBLE);
        mProfileImageEdit.setVisibility(View.INVISIBLE);
    }

    @Override
    public void resetProfileImage() {

        mProfileImage.setImageBitmap(null);
        mProfileImageEmpty.setVisibility(View.VISIBLE);
        mProfileImageEdit.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFirstNameInvalid() {
        Toast.makeText(getContext(), "First Name Invalid", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLastNameInvalid() {
        Toast.makeText(getContext(), "Last Name Invalid", Toast.LENGTH_LONG).show();
    }

    @Override
    public SignUpView getParentView() {
        return (SignUpView) getActivity();
    }
}
