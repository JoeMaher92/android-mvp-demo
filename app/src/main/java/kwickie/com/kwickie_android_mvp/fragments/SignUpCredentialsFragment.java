package kwickie.com.kwickie_android_mvp.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import kwickie.com.kwickie_android_mvp.R;
import kwickie.com.kwickie_android_mvp.activites.SignUpActivity;
import kwickie.com.kwickie_android_mvp.interfaces.presenters.SignUpCredentialsPresenter;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpCredentialsView;
import kwickie.com.kwickie_android_mvp.interfaces.views.SignUpView;
import kwickie.com.kwickie_android_mvp.presenters.SignUpCredentialsPresenterImpl;

/**
 * Created by joemaher on 1/05/16.
 */
public class SignUpCredentialsFragment extends Fragment implements SignUpCredentialsView {

    private SignUpCredentialsPresenter mPresenter;
    private EditText mEmailText;
    private EditText mPasswordText;
    private ImageView mProfileImage;
    private ImageView mProfileImageEmpty;
    private ImageView mProfileImageEdit;
    private ImageView mPasswordReveal;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_sign_up_credentials, container, false);

        mPresenter = new SignUpCredentialsPresenterImpl(this);

        mEmailText = (EditText) rootview.findViewById(R.id.email_text);
        mPasswordText = (EditText) rootview.findViewById(R.id.password_text);
        mProfileImage = (ImageView) rootview.findViewById(R.id.profile_image);
        mProfileImageEmpty = (ImageView) rootview.findViewById(R.id.profile_image_empty);
        mProfileImageEdit = (ImageView) rootview.findViewById(R.id.profile_image_edit);
        mPasswordReveal = (ImageView) rootview.findViewById(R.id.reveal);

        mPasswordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_NEXT) {
                    onContinuePressed();
                    return true;
                }

                return false;
            }
        });

        mPasswordReveal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int startPos = mPasswordText.getSelectionStart();
                int endPos = mPasswordText.getSelectionEnd();
                boolean transformed = mPasswordText.getTransformationMethod() instanceof PasswordTransformationMethod;

                mPresenter.revealPasswordClicked(startPos, endPos, transformed);
            }
        });

        String savedEmail = null;
        String savedPassword = null;

        if (getArguments() != null){

            savedEmail = getArguments().getString(SignUpActivity.FRAGMENT_ARG_EMAIL, null);
            savedPassword = getArguments().getString(SignUpActivity.FRAGMENT_ARG_PASSWORD, null);
        }

        mPresenter.createView(savedEmail, savedPassword);

        return rootview;
    }

    @Override
    public void onDestroyView() {

        String email = mEmailText.getText().toString();
        String password = mPasswordText.getText().toString();

        mPresenter.destroyView(email, password);
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sign_up_email, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_continue:
                onContinuePressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onContinuePressed() {

        String email = mEmailText.getText().toString();
        String password = mPasswordText.getText().toString();

        mPresenter.validateCredentials(email, password);
    }

    @Override
    public void revealPassword(int startPos, int endPos) {

        mPasswordText.setTransformationMethod(null);
        mPasswordText.setSelection(startPos, endPos);
        mPasswordReveal.setImageResource(R.drawable.ic_visibility_off);
    }

    @Override
    public void hidePassword(int startPos, int endPos) {

        mPasswordText.setTransformationMethod(new PasswordTransformationMethod());
        mPasswordText.setSelection(startPos, endPos);
        mPasswordReveal.setImageResource(R.drawable.ic_visibility);
    }

    @Override
    public void showEmailInvalid() {
        Toast.makeText(getContext(), "Email Invalid", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPasswordInvalid() {
        Toast.makeText(getContext(), "Password Invalid", Toast.LENGTH_LONG).show();
    }

    @Override
    public void fillEmail(String email) {
        mEmailText.setText(email);
    }

    @Override
    public void fillPassword(String password) {
        mPasswordText.setText(password);
    }

    @Override
    public SignUpView getParentView() {
        return (SignUpView) getActivity();
    }

    @Override
    public void displayProfileImage(String profileImageFilePath) {

        Bitmap bitmap = BitmapFactory.decodeFile(profileImageFilePath);

        mProfileImage.setImageBitmap(bitmap);
        mProfileImageEmpty.setVisibility(View.INVISIBLE);
        mProfileImageEdit.setVisibility(View.INVISIBLE);
    }

    @Override
    public void resetProfileImage() {

        mProfileImage.setImageBitmap(null);
        mProfileImageEmpty.setVisibility(View.VISIBLE);
        mProfileImageEdit.setVisibility(View.VISIBLE);
    }
}
