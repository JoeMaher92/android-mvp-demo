# README #

An App that demonstrates how we could implement the MVP Pattern within our current Kwickie Application

### Helpful reads ###

* [Organisaing the Presentation layer](http://antonioleiva.com/mvp-android/)
* [Dependency Injection with Dagger(3 parts)](http://antonioleiva.com/dependency-injection-android-dagger-part-1/)
* [Architecting Android... the clean way?](http://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/)
* [Dagger 2 scopes](http://frogermcs.github.io/dependency-injection-with-dagger-2-custom-scopes/)

### Includes ###

* Onboarding flow with MVP pattern and Dependency Injection in mind
* SignUpPreenter unit tests
* A few instrumentation tests for SignUpActivity